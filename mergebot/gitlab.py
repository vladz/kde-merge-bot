import httpx
import json

from .exceptions import GitlabError


def _serialize_param(value):
    if isinstance(value, bool):
        return "true" if value else "false"
    return str(value)


class Client:
    def __init__(self, address: str, auth_token: str):
        self.address = address
        self.auth_token = auth_token

    async def roundtrip(self, method, endpoint, **kwargs):
        url = self.address.rstrip('/') + '/api/v4' + endpoint
        headers = {'PRIVATE-TOKEN': self.auth_token}
        params = { key: _serialize_param(value) for key, value in kwargs.items() }

        response = method(url, headers=headers, timeout=60, params=params)
        if response.status_code == 204:
            return None
        elif response.status_code < 300:
            return json.loads(response.text)
        else:
            raise GitlabError(response.status_code)

    async def get(self, endpoint, **kwargs):
        return await self.roundtrip(httpx.get, endpoint, **kwargs)

    async def put(self, endpoint, **kwargs):
        return await self.roundtrip(httpx.put, endpoint, **kwargs)

    async def post(self, endpoint, **kwargs):
        return await self.roundtrip(httpx.post, endpoint, **kwargs)


class Object:
    def __init__(self, client, info):
        self.client = client
        self.info = info


class User(Object):
    @classmethod
    async def me(cls, client):
        user_info = await client.get("/user")
        return cls(client, user_info)

    @property
    def id(self):
        return self.info['id']

    @property
    def name(self):
        return self.info["name"]

    @property
    def email(self):
        return self.info["email"]


class Branch(Object):
    @classmethod
    async def get(cls, client, project_id, branch):
        branch_info = await client.get(f"/projects/{project_id}/repository/branches/{branch}")
        return cls(client, branch_info)

    @property
    def id(self):
        return self.info['id']


class Pipeline(Object):
    @property
    def web_url(self):
        return self.info["web_url"]

    @classmethod
    async def create(cls, client, project_id, branch):
        pipeline_info = await client.post(f"/projects/{project_id}/pipeline", ref=branch)
        return cls(client, pipeline_info)


class Project(Object):
    @property
    def http_url_to_repo(self):
        return self.info["http_url_to_repo"]

    @classmethod
    async def get(cls, client, project_id):
        project_info = await client.get(f"/projects/{project_id}")
        return cls(client, project_info)


class MergeRequest(Object):
    @property
    def author_id(self):
        return self.info['author']['id']

    @property
    def project_id(self):
        return self.info['project_id']

    @property
    def iid(self):
        return self.info['iid']

    @property
    def state(self):
        return self.info['state']

    @property
    def draft(self):
        return self.info['draft']

    @property
    def source_branch(self):
        return self.info['source_branch']

    @property
    def target_branch(self):
        return self.info['target_branch']

    @property
    def web_url(self):
        return self.info['web_url']

    async def comment(self, text):
        return await self.client.post(f"/projects/{self.project_id}/merge_requests/{self.iid}/notes",
                                      body=text)

    async def merge(self, merge_when_pipeline_succeeds, should_remove_source_branch):
        return await self.client.put(f"/projects/{self.project_id}/merge_requests/{self.iid}/merge",
                                     merge_when_pipeline_succeeds=merge_when_pipeline_succeeds,
                                     should_remove_source_branch=should_remove_source_branch)

    async def cancel_merge(self):
        return await self.client.post(f"/projects/{self.project_id}/merge_requests/{self.iid}/cancel_merge_when_pipeline_succeeds")

    async def rebase(self):
        return await self.client.put(f"/projects/{self.project_id}/merge_requests/{self.iid}/rebase")

    async def poll(self):
        self.info = await self.client.get(f"/projects/{self.project_id}/merge_requests/{self.iid}",
                                          include_rebase_in_progress=True)

    async def update(self, **kwargs):
        self.info = await self.client.put(f"/projects/{self.project_id}/merge_requests/{self.iid}", **kwargs)

    @classmethod
    async def get(cls, client, project_id, iid):
        mr_info = await client.get(f"/projects/{project_id}/merge_requests/{iid}")
        return cls(client, mr_info)

    @classmethod
    async def create(cls, client, project_id, **kwargs):
        mr_info = await client.post(f"/projects/{project_id}/merge_requests", **kwargs)
        return cls(client, mr_info)

    @classmethod
    async def assigned(cls, client):
        results = await client.get("/merge_requests", scope="assigned_to_me", state="opened")
        mrs = []
        for result in results:
            mrs.append(await cls.get(client, result["project_id"], result["iid"]))
        return mrs
