import http


class RebaseError(Exception):
    """Raised by the MergeJob if the specified branch cannot
    be rebased, for example if there are merge conflicts.
    """
    pass


class MergeError(Exception):
    """Raised by the MergeJob if the specified branch cannot
    be merged.
    """
    pass


class CancelledError(Exception):
    """Raised by cancellable() when the cancel event is set.
    """
    pass


class GitError(Exception):
    """Raised by Repository when working with it.
    """
    pass


class GitlabError(Exception):
    """Raised when calling into GitLab REST API.
    """

    def __init__(self, status_code):
        self.status_code = status_code

    def __str__(self):
        return http.HTTPStatus(self.status_code).description
