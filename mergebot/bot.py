import asyncio
import collections
import logging
import re

from .coro import cancellable
from .exceptions import CancelledError
from .job import JobStatus, CherryPickJob, MergeJob
from .gitlab import Client, MergeRequest, Project, User
from .webhook import Webhook


logger = logging.getLogger(__name__)


async def merge_worker(queue: asyncio.Queue):
    while True:
        job = await queue.get()
        try:
            await job.execute()
        except Exception:
            job.set_failed()
        else:
            job.set_completed()
        finally:
            queue.task_done()


Worker = collections.namedtuple("Worker", ["task", "queue"])
JobHandle = collections.namedtuple("JobHandle", ["project_id", "iid"])

BotConfig = collections.namedtuple("BotConfig", [
    "gitlab_url",
    "gitlab_auth_token",
    "webhook_address",
    "webhook_port",
    "webhook_secret",
])


def extract_cherry_pick_to(text: str):
    cherry_pick = set()
    for line in text.splitlines():
        if line.startswith("Pick-to:"):
            for branch_name in line.lstrip("Pick-to:").split():
                cherry_pick.add(branch_name)
    return cherry_pick


class Bot:
    def __init__(self, config: BotConfig):
        self.config = config
        self.me = None
        self.client = None
        self.workers = dict()
        self.jobs = dict()
        self.webhook = Webhook(self.on_webhook,
                               config.webhook_port,
                               config.webhook_address,
                               config.webhook_secret)

    async def start(self):
        await self.webhook.start()

        self.client = Client(self.config.gitlab_url, self.config.gitlab_auth_token)
        self.me = await User.me(self.client)

        mrs = await MergeRequest.assigned(self.client)
        for mr in mrs:
            self.schedule_merge_job(mr)

    def get_worker_for_mr(self, mr: MergeRequest) -> Worker:
        worker = self.workers.get(mr.project_id)
        if worker:
            return worker

        queue = asyncio.Queue()
        task = asyncio.create_task(merge_worker(queue))
        worker = Worker(task, queue)
        self.workers[mr.project_id] = worker
        return worker

    async def get_repo_url(self, project_id):
        project = await Project.get(self.client, project_id)

        pattern = "(http(s)?://)"
        repl = r"\1" + "oauth2:" + self.config.gitlab_auth_token + "@"
        return re.sub(pattern, repl, project.http_url_to_repo)

    async def schedule_cherry_pick_job(self, merge_job: MergeJob, branch_name: str):
        logger.info(f"Scheduling cherry pick job for {branch_name}")

        try:
            await cancellable(merge_job.done_event.wait(), merge_job.cancel_event.wait())
        except CancelledError:
            logger.info(f"Cancelling cherry pick job for {branch_name}")
            return

        if merge_job.status != JobStatus.Completed:
            return

        worker = self.get_worker_for_mr(merge_job.merge_request)
        repo_url = await self.get_repo_url(merge_job.merge_request.project_id)
        worker.queue.put_nowait(CherryPickJob(merge_job.merge_request, repo_url, branch_name, self.me))

    async def autoremove_merge_job(self, handle: JobHandle, job: MergeJob):
        await job.done_event.wait()
        self.jobs.pop(handle, None)

    def schedule_merge_job(self, mr: MergeRequest):
        # If the branch cannot be merged, assign the merge request to the author.
        # In case the MR is created by the bot, then simplify unassign it.
        fallback_assignee = 0
        if mr.author_id != self.me.id:
            fallback_assignee = mr.author_id

        job = MergeJob(mr, fallback_assignee)

        handle = JobHandle(mr.project_id, mr.iid)
        self.jobs[handle] = job
        asyncio.create_task(self.autoremove_merge_job(handle, job))

        description = mr.info.get("description")
        if description:
            cherry_pick_to = extract_cherry_pick_to(description)
            for branch_name in cherry_pick_to:
                asyncio.create_task(self.schedule_cherry_pick_job(job, branch_name))

        worker = self.get_worker_for_mr(mr)
        worker.queue.put_nowait(job)

    def on_webhook(self, info):
        if info["object_kind"] == "merge_request":
            attrs = info["object_attributes"]
            if attrs["action"] in ("open", "reopen"):
                self.on_merge_request_open(info)
            elif attrs["action"] == "close":
                self.on_merge_request_close(info)
            elif attrs["action"] == "update":
                self.on_merge_request_update(info)

    def on_merge_request_open(self, data):
        if self.me.id in data["object_attributes"]["assignee_ids"]:
            asyncio.create_task(self.create_merge_job(data))

    def on_merge_request_close(self, data):
        if self.me.id in data["object_attributes"]["assignee_ids"]:
            asyncio.create_task(self.remove_merge_job(data))

    def on_merge_request_update(self, data):
        if data["object_attributes"]["state"] != "opened":
            return

        if "assignees" in data["changes"]:
            previous = False
            current = False

            changeset = data["changes"]["assignees"]
            for user in changeset["previous"]:
                if user["id"] == self.me.id:
                    previous = True
                    break
            for user in changeset["current"]:
                if user["id"] == self.me.id:
                    current = True
                    break

            if previous != current:
                if previous:
                    asyncio.create_task(self.remove_merge_job(data))
                else:
                    asyncio.create_task(self.create_merge_job(data))

    async def create_merge_job(self, data):
        logger.info(f"Scheduling merge job for {data['object_attributes']['url']}")

        project_id = data['project']['id']
        iid = data['object_attributes']['iid']
        self.schedule_merge_job(await MergeRequest.get(self.client, project_id, iid))

    async def remove_merge_job(self, data):
        logger.info(f"Unscheduling merge job for {data['object_attributes']['url']}")

        project_id = data['project']['id']
        iid = data['object_attributes']['iid']

        job_handle = JobHandle(project_id, iid)
        job = self.jobs.get(job_handle)
        if job:
            job.set_cancelled()
