import asyncio
import http
import json
import logging
import tornado.web


logger = logging.getLogger(__name__)


class WebhookHandler(tornado.web.RequestHandler):
    def initialize(self, hook, secret_token):
        self.hook = hook
        self.secret_token = secret_token

    async def post(self) -> None:
        # we want json
        content_type = self.request.headers.get("Content-Type", None)
        if content_type != "application/json":
            raise tornado.web.HTTPError(http.HTTPStatus.FORBIDDEN)

        # check that no random endpoint sends us data
        secret_token = self.request.headers.get("X-Gitlab-Token", None)
        if secret_token != self.secret_token:
            logger.warning(f"Unrecognized X-Gitlab-Token {secret_token} received from {repr(self.request)}")
            raise tornado.web.HTTPError(http.HTTPStatus.FORBIDDEN)

        self.hook(json.loads(self.request.body))


class Webhook:
    def __init__(self, hook, port, address, secret):
        self.port = port
        self.address = address
        self.secret = secret
        self.hook = hook
        self.app = None

    async def start(self):
        self.app = tornado.web.Application([
            (r"/webhook", WebhookHandler, dict(hook=self.hook, secret_token=self.secret))
        ])
        self.app.listen(self.port, self.address)
        logger.warning(f"Started webhook on {self.address}:{self.port}")
