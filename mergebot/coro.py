import asyncio

from .exceptions import CancelledError


async def cancellable(coro, cancel_coro):
    tasks = [
        asyncio.create_task(coro),
        asyncio.create_task(cancel_coro)
    ]
    done, pending = await asyncio.wait(tasks, return_when=asyncio.FIRST_COMPLETED)
    for task in pending:
        task.cancel()
    for task in done:
        if task == tasks[0]:
            return task.result()
        else:
            raise CancelledError()
