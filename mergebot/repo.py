import asyncio
import logging
import subprocess

from .exceptions import GitError


logger = logging.getLogger(__name__)


class Repository:
    def __init__(self, origin_url: str, local_path: str):
        self.origin_url = origin_url
        self.local_path = local_path

    async def clone(self):
        await self.git('clone', '--origin=origin', self.origin_url, self.local_path)

    async def configure(self, user_name, user_email):
        await self.git('config', 'user.name', user_name)
        await self.git('config', 'user.email', user_email)

    async def fetch(self):
        await self.git('fetch', '--prune', 'origin', working_directory=self.local_path)

    async def cherry_pick(self, start_hash: str, end_hash: str):
        await self.git('cherry-pick', '-x', f'{start_hash}..{end_hash}', working_directory=self.local_path)

    async def checkout_branch(self, branch_name: str, source_branch: str):
        await self.git('checkout', '-B', branch_name, source_branch, working_directory=self.local_path)

    async def remove_branch(self, branch_name: str):
        await self.git('branch', '-D', branch_name, working_directory=self.local_path)

    async def push(self, branch_name: str):
        await self.git('push', 'origin', branch_name, '-o', 'ci.skip', working_directory=self.local_path)

    async def git(self, *args, working_directory=None):
        git_args = []
        if working_directory:
            git_args.extend(['-C', working_directory])
        git_args.extend([arg for arg in args if str(arg)])

        process = await asyncio.create_subprocess_exec('git', *git_args,
                                                        stdout=asyncio.subprocess.PIPE,
                                                        stderr=asyncio.subprocess.PIPE)
        stdout, stderr = await process.communicate()

        if stdout:
            logger.debug(stdout.decode())

        if process.returncode != 0:
            raise GitError(stderr.decode())
