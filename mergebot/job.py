import asyncio
import enum
import logging
import tempfile

from .coro import cancellable
from .exceptions import CancelledError, GitError, GitlabError, RebaseError, MergeError
from .gitlab import Branch, MergeRequest, Pipeline, User
from .repo import Repository


logger = logging.getLogger(__name__)


class JobStatus(enum.Enum):
    """This enum describes the current status of a job.
    """

    Running = 0
    Cancelled = 1
    Failed = 2
    Completed = 3


class Job:
    """The base class for merge and cherry-picking job classes.
    """

    def __init__(self):
        self.done_event = asyncio.Event()
        self.cancel_event = asyncio.Event()
        self.status = JobStatus.Running

    def set_cancelled(self):
        if self.status == JobStatus.Running:
            self.status = JobStatus.Cancelled
            self.cancel_event.set()

    def set_failed(self):
        if self.status == JobStatus.Running:
            self.status = JobStatus.Failed
            self.done_event.set()

    def set_completed(self):
        if self.status == JobStatus.Running:
            self.status = JobStatus.Completed
            self.done_event.set()

    async def execute(self):
        raise NotImplementedError("not implemented")

    async def with_cancel(self, aw):
        return await cancellable(aw, self.cancel_event.wait())


class MergeJob(Job):
    """Attempts to merge the specified merge request. The source branch will be
    rebased if needed and scheduled for merge when the pipeline succeeds.
    """

    def __init__(self, merge_request, default_assignee_id=None):
        super().__init__()
        self.merge_request = merge_request
        self.fallback_assignee_id = default_assignee_id
        self.poll_rate = 5

    async def ensure_rebased(self):
        """Ensures that the merged branch is rebased on target branch.

        If the source branch is rebased, it will also wait until the corresponding
        pipeline is kicked off.

        If there's a merge conflict, the RebaseError() exception will be thrown.
        """

        old_head_pipeline = self.merge_request.info.get("head_pipeline")
        old_diff_refs = self.merge_request.info.get('diff_refs')

        # Check if the merge request branch is already up to date.
        target_branch = await self.with_cancel(Branch.get(self.merge_request.client,
                                                          self.merge_request.project_id,
                                                          self.merge_request.target_branch))
        if target_branch.info['commit']['id'] == old_diff_refs['start_sha']:
            return # nothing to do

        await self.with_cancel(self.merge_request.rebase())

        while True:
            # Wait until the rebase is complete. The merge PUT request may fail if we issue
            # it before the rebase is finished.
            await self.with_cancel(self.merge_request.poll())

            rebase_in_progress = self.merge_request.info.get('rebase_in_progress')
            if rebase_in_progress:
                await self.with_cancel(asyncio.sleep(self.poll_rate))
                continue

            merge_error = self.merge_request.info.get('merge_error')
            if merge_error:
                raise RebaseError("Cannot rebase the branch." + merge_error)

            # If the branch has been rebased, wait until the corresponding pipeline is started.
            head_pipeline = self.merge_request.info.get("head_pipeline")
            if head_pipeline == old_head_pipeline:
                await self.with_cancel(asyncio.sleep(self.poll_rate))
                continue

            break  # It's all good

    async def ensure_pipeline(self):
        """Ensures that the pipeline has been run at least onces for the current head.
        """
        accepted_statuses = (
            "created",
            "waiting_for_resource",
            "preparing",
            "pending",
            "running",
            "success"
        )
        head_pipeline = self.merge_request.info.get("head_pipeline")
        if head_pipeline and head_pipeline["sha"] == self.merge_request.info["sha"] and head_pipeline["status"] in accepted_statuses:
            return

        try:
            pipeline = await Pipeline.create(self.merge_request.client, self.merge_request.project_id, self.merge_request.source_branch)
            logger.info(f"Started pipeline {pipeline.web_url} for {self.merge_request.web_url}")
        except GitlabError as err:
            raise MergeError("Cannot start a new pipeline: " + str(err))

    async def try_merge(self):
        """Attempts to merge the given branch. If the source branch cannot be merged,
        the MergeError() exception will be thrown and a comment on the merge request page
        will be created.
        """

        try:
            await self.with_cancel(self.merge_request.merge(merge_when_pipeline_succeeds=True,
                                                            should_remove_source_branch=True))
        except CancelledError:
            return
        except GitlabError as err:
            if err.status_code == 401:
                raise MergeError("I do not have permissions to merge this branch")
            elif err.status_code == 405:
                if self.merge_request.draft:
                    raise MergeError("The merge request has been marked draft")
                if self.merge_request.state == "closed":
                    raise MergeError("The merge request has been closed in meanwhile")
                elif self.merge_request.state == "merged":
                    raise MergeError("The merge request has been merged in meanwhile")
                else:
                    raise MergeError("GitLab refused to merge this branch and I don't know why")
            elif err.status_code == 406:
                raise MergeError("This branch has conflicts and cannot be merged")

        try:
            while True:
                # Wait until the merge request is actually merged.
                await self.with_cancel(self.merge_request.poll())

                if self.merge_request.state == "merged":
                    return
                elif self.merge_request.state == "closed":
                    raise MergeError("Merge request has been closed")

                merge_status = self.merge_request.info['merge_status']
                if merge_status != "can_be_merged":
                    raise MergeError("Merge request cannot be merged")

                merge_when_pipeline_succeeds = self.merge_request.info['merge_when_pipeline_succeeds']
                if not merge_when_pipeline_succeeds:
                    raise MergeError("Canceled")

                await self.with_cancel(asyncio.sleep(self.poll_rate))
        except CancelledError:
            # The merge request has been cancelled while we are waiting for ci to complete running.
            await self.merge_request.cancel_merge()

    async def execute(self):
        if self.cancel_event.is_set():
            return

        try:
            await self.ensure_rebased()
            await self.ensure_pipeline()
            await self.try_merge()
        except (RebaseError, MergeError) as err:
            await self.merge_request.comment(str(err))
            await self.merge_request.update(assignee_id=self.fallback_assignee_id)
            raise


class CherryPickJob(Job):
    """Attempts to cherry-pick the specified merge request to the target branch and
    create a new merge request, which will be eventually handled by MergeJob.
    """

    def __init__(self, merge_request: MergeRequest, repo_url: str, target_branch: str, assignee: User):
        super().__init__()
        self.merge_request = merge_request
        self.repo_url = repo_url
        self.target_branch = target_branch
        self.assignee = assignee

    async def execute(self):
        logger.info(f"Cherry picking {self.merge_request.web_url} to {self.target_branch}")

        branch_name = f"work/ci/cherry-pick-{self.merge_request.iid}"
        diff_refs = self.merge_request.info["diff_refs"]
        labels = self.merge_request.info["labels"]
        original_title = self.merge_request.info["title"]

        try:
            with tempfile.TemporaryDirectory() as tmpdir:
                repo = Repository(self.repo_url, tmpdir)
                await self.with_cancel(repo.clone())
                await self.with_cancel(repo.configure(user_name=self.assignee.name,
                                                      user_email=self.assignee.email))
                await self.with_cancel(repo.checkout_branch(branch_name, f"origin/{self.target_branch}"))
                await self.with_cancel(repo.cherry_pick(diff_refs["base_sha"], diff_refs["head_sha"]))
                await self.with_cancel(repo.push(branch_name))
        except CancelledError:
            return
        except GitError as err:
            logger.warning(f"Failed to cherry-pick {self.merge_request.web_url} to {self.target_branch}")
            await self.merge_request.comment("Failed to cherry-pick this branch: " + str(err))
            raise

        try:
            pick_mr = await MergeRequest.create(self.merge_request.client,
                                                project_id=self.merge_request.project_id,
                                                source_branch=branch_name,
                                                target_branch=self.target_branch,
                                                title=f"Cherry-pick \"{original_title}\"",
                                                description=f"!{self.merge_request.iid}",
                                                labels=",".join(labels),
                                                assignee_id=self.assignee.id,
                                                allow_collaboration=True,
                                                remove_source_branch=True)
            logger.info(f"Successfully cherry-picked {self.merge_request.web_url}. Go to {pick_mr.web_url}")
        except GitlabError as err:
            error_message = f"Failed to create cherry-pick merge request for {branch_name}: " + str(err)
            logger.warning(error_message)
            await self.merge_request.comment(error_message)
            raise
