# kde-merge-bot

Basic merge bot for GitLab.

To use it, assign a merge request to it and the bot will attempt to get it merged.

If the merge request description contains special keyword `Pick-to`, it will try
to cherry-pick commits in the MR to the specified branches. The branch names in
`Pick-to` are separated by empty space, e.g. `Pick-to: Plasma/5.24 Plasma/5.25`.


### Example usage

```
python3 mergebot.app --debug --config config.yml
```


## Configuration

### config.yml

```yml
gitlab_url: "https://invent.kde.org"
gitlab_auth_token: "my secret secret auth token"
webhook_address: "localhost"
webhook_port: 3030
webhook_secret: "my secret secret webhook token"
```

The value of `gitlab_auth_token` and `webhook_secret` can be also specified using
the `MERGEBOT_GITLAB_TOKEN` and the `MERGEBOT_WEBHOOK_TOKEN` environment variable,
respectively.


### Webhooks

In order to operate, this bot needs the following webhooks:

* Merge requests events
